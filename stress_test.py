from time import sleep
import stressinjector as injector
from cpu import CPUStress

injector.CPUStress = CPUStress

if __name__ == '__main__':
    for i in range(1,21):
        CPUStress(seconds=10, cores=i)
        sleep(10)